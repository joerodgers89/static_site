def lambda_handler(event, context):
    # Changed via CI/CD
    print("Hello Joe and Troy someone is using your lambda")
    threes = ', '.join([str(i) for i in range(3, 31, 3)])
    response ={
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*",
        },
        "body": f"Here's the 3 times table for no reason: {threes}"
    }

    return response